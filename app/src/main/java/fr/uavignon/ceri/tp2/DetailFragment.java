package fr.uavignon.ceri.tp2;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailFragment extends Fragment {

    private EditText textTitle, textAuthors, textYear, textGenres, textPublisher;
    private DetailViewModel viewModel;

    private Book modifiedBook;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        // Get selected book
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        viewModel.findBook(args.getBookId());

        observerSetup();

        view.findViewById(R.id.buttonUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modifiedBook.title = textTitle.getText().toString();
                modifiedBook.authors = textAuthors.getText().toString();
                modifiedBook.year = textYear.getText().toString();
                modifiedBook.genres = textGenres.getText().toString();
                modifiedBook.publisher = textPublisher.getText().toString();

                if(
                        modifiedBook.title.matches("") ||
                        modifiedBook.authors.matches("")
                ){
                    Snackbar.make(view, "Veuillez remplir le titre et les auteurs", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }else{
                    viewModel.insertOrUpdateBook(modifiedBook);
                    NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                            .navigate(R.id.action_SecondFragment_to_FirstFragment);
                }

            }
        });

        view.findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }

    private  void observerSetup(){
        viewModel.getSelectedBook().observe(
                getViewLifecycleOwner(),
                new Observer<Book>(){
                    @Override
                    public void onChanged(Book book){




                        textTitle = (EditText) getView().findViewById(R.id.nameBook);
                        textAuthors = (EditText) getView().findViewById(R.id.editAuthors);
                        textYear = (EditText) getView().findViewById(R.id.editYear);
                        textGenres = (EditText) getView().findViewById(R.id.editGenres);
                        textPublisher = (EditText) getView().findViewById(R.id.editPublisher);

                        if(book != null) {
                            modifiedBook = book;
                            textTitle.setText(book.getTitle());
                            textAuthors.setText(book.getAuthors());
                            textYear.setText(book.getYear());
                            textGenres.setText(book.getGenres());
                            textPublisher.setText(book.getPublisher());
                        }else{
                            modifiedBook = new Book("", "", "", "", "");
                        }
                    }
                }
            );

    }
}
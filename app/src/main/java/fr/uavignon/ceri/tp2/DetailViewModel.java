package fr.uavignon.ceri.tp2;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;


import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.data.BookRepository;

public class DetailViewModel extends AndroidViewModel {


    private BookRepository repository;
    private MutableLiveData<Book> selectedBook;

    public DetailViewModel(@NonNull Application application) {
        super(application);
        repository = new BookRepository(application);
        selectedBook = repository.getSelectedBook();
    }

    MutableLiveData<Book> getSelectedBook(){
        return selectedBook;
    }

    void findBook(long id){
        repository.getBook(id);
    }

    void insertOrUpdateBook(Book book){
        if(selectedBook.getValue() == null){
            repository.insertBook(book);
        }else{
            repository.updateBook(book);
        }
    }
}

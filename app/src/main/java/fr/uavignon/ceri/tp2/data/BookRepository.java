package fr.uavignon.ceri.tp2.data;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ExecutorService;

import static fr.uavignon.ceri.tp2.data.BookRoomDatabase.databaseWriteExecutor;

public class BookRepository {
    private MutableLiveData<Book> selectedBook =
            new MutableLiveData<>();
    private LiveData<List<Book>> allBooks;

    private BookDao bookDao;


    public BookRepository(Application application) {
        BookRoomDatabase db = BookRoomDatabase.getDatabase(application);
        bookDao = db.bookDao();
        allBooks = bookDao.getAllBooks();
    }

    public void insertBook(Book newBook) {
        databaseWriteExecutor.execute(() -> {
            bookDao.insertBook(newBook);
        });
    }

    public void deleteBook(long id) {
        databaseWriteExecutor.execute(() -> {
            bookDao.deleteBook(id);
        });
    }

    public void deleteAllBooks() {
        databaseWriteExecutor.execute(() -> {
            bookDao.deleteAllBooks();
        });
    }

    public void updateBook(Book book){
        databaseWriteExecutor.execute(() -> {
            bookDao.updateBook(book);
        });
    }

    public void getBook(long id) {

        Future<Book> fBook = databaseWriteExecutor.submit(() -> {
            return bookDao.getBook(id);
        });
        try {
            selectedBook.setValue(fBook.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }

    public MutableLiveData<Book> getSelectedBook() {
        return selectedBook;
    }


}
